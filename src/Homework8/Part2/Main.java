package Homework8.Part2;

import java.util.ArrayList;

/**
 * Created by p.kovarsky on 12/21/16.
 */
public class Main {

    public static void main(String[] args) {
        ArrayList<Article> l = new ArrayList();
        ArrayList<Article> s = new ArrayList();
        Article articleLOne = new Article("Nature", "16.05.2007", new String[]{"Tag1", "Tag2"});
        Article articleLTwo = new Article("Winter", "12.06.2017", new String[]{"Tag3", "Tag4"});
        Article articleSOne = new Article("Summer", "16.05.2007", new String[]{"Tag1", "Tag2"});
        Article articleSTwo = new Article("Science", "12.06.2017", new String[]{"Tag3", "Tag4"});
        Article articleSThree = new Article("Nature", "16.05.2007", new String[]{"Tag3", "Tag4"});
        l.add(0, articleLOne);
        l.add(1, articleLTwo);
        s.add(0, articleSOne);
        s.add(1, articleSTwo);
        s.add(2, articleSThree);

        for (int i = 0; i < l.size(); i++) {
            for (int j = 0; j < s.size(); j++) {
                if (l.get(i).title == s.get(j).title && l.get(i).date == s.get(j).date) {
                    System.out.println(l.get(i).title);
                }
            }
        }
    }
}
