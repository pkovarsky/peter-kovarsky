package Homework8.Part2;


import java.util.Date;

public class Article {

    String title;
    String date;
    String[] tags;

    public Article(String title, String date, String[] tags) {
        this.title = title;
        this.date = date;
        this.tags = tags;
    }
}
