package Homework8.Part3;


import java.util.Arrays;

public class Literature {

    String title;
    String[] tag;

    public Literature(String title, String[] tag) {
        this.title = title;
        this.tag = tag;
    }

    @Override
    public String toString() {
        return title + " " + Arrays.toString(tag);
    }
}
