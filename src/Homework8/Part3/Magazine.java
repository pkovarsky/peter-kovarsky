package Homework8.Part3;

/**
 * Created by p.kovarsky on 12/21/16.
 */
public class Magazine extends Literature {

    String publishingHouse;

    public Magazine(String title, String[] tag, String publishingHouse) {
        super(title, tag);
        this.publishingHouse = publishingHouse;
    }

    @Override
    public String toString() {
        return super.toString() + " " + publishingHouse;
    }
}
