package Homework8.Part3;


import com.sun.org.apache.bcel.internal.generic.NEW;

import java.util.ArrayList;

public class Main {

    static String[] tags = {"love", "breaking news", "girls"};
    static ArrayList<Literature> requestLiterature = new ArrayList();

    public static void main(String[] args) {

        ArrayList<Literature> newspaper = new ArrayList();
        ArrayList<Literature> book = new ArrayList();
        ArrayList<Literature> magazine = new ArrayList();
        Newspaper newspaperOne = new Newspaper("New york times", new String[]{"breaking news", "adventure", "sport"}, "16.05.1993");
        Newspaper newspaperTwo = new Newspaper("BBC", new String[]{"breaking news", "politic", "economic"}, "19.09.2007");
        Newspaper newspaperThree = new Newspaper("Hunters", new String[]{"Bears", "Ducks", "Winter"}, "27.02.2016");
        Book bookOne = new Book("Adventure", new String[]{"adventure", "earth", "national"}, "Vladimir");
        Book bookTwo = new Book("Love Story", new String[]{"love", "relationships", "peoples"}, "Vladislav");
        Book bookThree = new Book("Winter vacation", new String[]{"winter", "sport", "vacation"}, "Nikolay");
        Magazine magazineOne = new Magazine("Shopping time", new String[]{"prices", "shops", "discount"}, "Kharkiv press");
        Magazine magazineTwo = new Magazine("Hustler", new String[]{"girls", "cars", "sport"}, "California times");
        Magazine magazineThree = new Magazine("Cooking", new String[]{"food", "kitchen", "cooking"}, "Cooking inc.");

        newspaper.add(newspaperOne);
        newspaper.add(newspaperTwo);
        newspaper.add(newspaperThree);

        book.add(bookOne);
        book.add(bookTwo);
        book.add(bookThree);

        magazine.add(magazineOne);
        magazine.add(magazineTwo);
        magazine.add(magazineThree);

        findTag(newspaper);
        findTag(book);
        findTag(magazine);

        for (Literature literature : requestLiterature) {
            System.out.println(literature.toString());
        }
    }

    static void findTag(ArrayList<Literature> obj) {

        for (int i = 0; i < obj.size(); i++) {
            for (int j = 0; j < tags.length; j++) {
                for (int k = 0; k < 3; k++) {
                    if (obj.get(i).tag[j] == tags[k]) {
                        requestLiterature.add(obj.get(i));
                    }
                }
            }
        }
    }
}




