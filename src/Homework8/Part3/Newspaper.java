package Homework8.Part3;

/**
 * Created by p.kovarsky on 12/21/16.
 */
public class Newspaper extends Literature {

    String date;

    public Newspaper(String title, String[] tag, String date) {
        super(title, tag);
        this.date = date;
    }

    @Override
    public String toString() {
        return super.toString() + " " + date;
    }
}
