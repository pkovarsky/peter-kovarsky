package Homework8.Part3;

/**
 * Created by p.kovarsky on 12/21/16.
 */
public class Book extends Literature {

    String author;

    public Book(String title, String[] tag, String author) {
        super(title, tag);
        this.author = author;
    }

    @Override
    public String toString() {
        return super.toString() + " " + author;
    }
}
