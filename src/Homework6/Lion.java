package Homework6;


public class Lion extends Cat {


    public Lion(String name, double weight, int height) {
        super(name, weight, height);
    }

    @Override
    public double getPowerFactor() {
        double p = (weight * height) * randInt(1, 4);
        return  p + p/20;
    }
}


