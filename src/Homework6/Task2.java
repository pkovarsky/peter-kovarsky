package Homework6;

public class Task2 {

    public static void main(String[] args) {

        //create cats
        Cat chiropractor = new Cat("Chip", 7, 50);
        Cat godzilla = new Cat("Godzilla", 9.8, 60);
        Cat lef = new Lion ("Lef", 10 ,70);

        //start fighting
        System.out.printf("%s vs %s", lef.name, godzilla.name).println();

        //get winner
        String winnerName = getWinner(lef, godzilla);

        //print winner name
        System.out.printf("%s wins", winnerName);
    }

    static String getWinner(Cat catInRedCorner, Cat catInBlackCorner) {
        if (catInBlackCorner.getPowerFactor() > catInRedCorner.getPowerFactor()) {
            return catInBlackCorner.name;
        } else {
            return catInRedCorner.name;
        }
    }
}
