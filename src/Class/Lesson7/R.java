package Class.Lesson7;


public class R extends F {

    double side;

    public R(String name, double side) {
        super(name);
        this.side = side;
    }

    @Override
    public double getS() {
        return side * side;
    }
}
