package Class.Lesson7;


public class C extends F {

    double r;

    public C(String name, double r) {
        super(name);
        this.r = r;
    }

    @Override
    public double getS() {
        return Math.PI * r * r;
    }
}
