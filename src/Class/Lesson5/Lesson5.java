package Class.Lesson5;

public class Lesson5 {
    static Obmenka[] obmenki = new Obmenka[4];

    public static void main(String[] args) {
        initObmenka();
        int kek = 0;
        double min = obmenki[0].usd;
//        Obmenka bestObmen;
        for (int i = 0; i < obmenki.length; i++) {
            if (obmenki[i].usd < min) {
                min = obmenki[i].usd;
                kek = i;
            }
        }
        System.out.println(obmenki[kek].city + " - Наименьший курс");
    }


    static void initObmenka() {
        for (int i = 0; i < obmenki.length; i++) {
            Obmenka newObmenka = new Obmenka();
            newObmenka.city = "City " + (i + 1);
            newObmenka.address = "Address " + (i + 1);
            newObmenka.usd = 20 + i;
            obmenki[i] = newObmenka;
        }
    }
}



