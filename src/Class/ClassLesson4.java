package Class;

public class ClassLesson4 {

    static Bank[] banks = new Bank[3];

    public static void main(String[] args) {


        initBanks();
        int uah = 5000;
        Bank myBank = getBestUSDCourse();
        double myUSD = uah / myBank.usd;
        System.out.println(myBank.name + " can get " +String.format("%.2f", myUSD) + "$");

    }

    static void initBanks(){
        for (int i = 0; i < banks.length; i++) {
            Bank newBank = new Bank();
            newBank.name = "Bank# " + (i+1);
            newBank.usd = 27.1 + 0.1*i;
            newBank.eur = 29.1 + 0.1*i;
            banks[i] = newBank;
        }
    }


    static Bank getBestUSDCourse(){
        Bank greatBank = banks[0];
        for (int i = 0; i < banks.length; i++) {
            if(greatBank.usd < banks[i].usd){
                greatBank = banks[i];
            }
        }
        return greatBank;
    }

    static class Bank {
        String name;
        double usd;
        double eur;

    }
}
