package Class;

public class ClassLesson3 {

    static String[] titles;

    public static void main(String[] args) {
        initTitles();
        printTitles(titles);

//        System.out.println(titles[0]);
//        System.out.println(titles[1]);
    }

    static void printTitles(String string[]) {
        for (int i = 0; i < string.length; i++) {
            System.out.println(string[i]);
        }
//        return string;
    }

    private static String[] initTitles() {
        titles = new String[5];
        for (int i = 0; i < 5; i++) {
            titles[i] = "Article " + i;
        }
        return titles;
    }
}