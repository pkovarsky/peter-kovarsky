package Homework7;


public class BaseAndPercent extends Accounting {

    double monthSalary;
    double percent;
    double firstYear;
    double secondYear;

    public BaseAndPercent(String name, String lastName, String position, double monthSalary, double percent, double firstYear, double secondYear) {
        super(name, lastName, position);
        this.monthSalary = monthSalary;
        this.percent = percent;
        this.firstYear = firstYear;
        this.secondYear = secondYear;
    }

    @Override
    public double getMonthSalary() {
        return monthSalary + percent * ((firstYear + secondYear) / 12);
    }

    @Override
    public double getYearSalary() {
        return getMonthSalary() * 12;
    }
}
