package Homework7;


public class Hour extends Accounting {

    double hourSalary;

    public Hour(String name, String lastName, String position, double hourSalary) {
        super(name, lastName, position);
        this.hourSalary = hourSalary;
    }

    @Override
    public double getMonthSalary() {
        return hourSalary * 40 * 4;
    }

    @Override
    public double getYearSalary() {
        return getMonthSalary() * 12;
    }
}
