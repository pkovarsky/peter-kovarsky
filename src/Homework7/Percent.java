package Homework7;


public class Percent extends Accounting {

    double percentSalary;
    double firstYear;
    double secondYear;

    public Percent(String name, String lastName, String position, double percentSalary, double firstYear, double secondYear) {
        super(name, lastName, position);
        this.percentSalary = percentSalary;
        this.firstYear = firstYear;
        this.secondYear = secondYear;
    }

    @Override
    public double getMonthSalary() {
        return percentSalary*((firstYear+secondYear)/12);
    }

    @Override
    public double getYearSalary() {
        return getMonthSalary()*12;
    }
}
