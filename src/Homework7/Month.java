package Homework7;


public class Month extends Accounting {

    double monthSalary;

    public Month(String name, String lastName, String position, double salary) {
        super(name, lastName, position);
        this.monthSalary = salary;
    }

    public double getMonthSalary() {
        return monthSalary;
    }

    @Override
    public double getYearSalary() {
        return monthSalary * 12;
    }


}
