package Homework7;


public class Main {

    public static void main(String[] args) {

        Accounting[] profiles = new Accounting[4];
        profiles[0] = new Month("Elena", "Ivanova", "Vice Director", 4500);
        profiles[1] = new Hour("Dmitriy", "Vakulenko", "Designer", 7);
        profiles[2] = new Percent("Anna", "Korenkova", "Sales Manager", 5, 50000, 65000);
        profiles[3] = new BaseAndPercent("Tatiana", "Sergeevna", "Sales manager", 1000, 3, 50000, 65000);

        for (Accounting profile : profiles) {
            System.out.println("\n" + profile.name + " " + profile.lastName + " " + profile.position + "\nполучает в месяц: " +
                    profile.getMonthSalary() + "\nполучает за год: " + profile.getYearSalary());
        }
    }
}
