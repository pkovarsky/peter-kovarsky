package Homework7;


public abstract class Accounting {

    String name;
    String lastName;
    String position;

    public Accounting(String name, String lastName, String position) {
        this.name = name;
        this.lastName = lastName;
        this.position = position;
    }

    public abstract double getMonthSalary();

    public abstract double getYearSalary();
}
