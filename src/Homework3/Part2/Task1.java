package Homework3.Part2;

import java.util.Locale;
import java.util.Scanner;

public class Task1 {

    /**
     * This program convert UAN to USD or EUR
     */
    public static void main(String[] args) {

        //init variables
        String USD = "USD";

        //init Banks
        String privat = "Privat";
        String pumb = "Pumb";
        String oshad = "Oshad";

        float COURSE_USD_Privat = 2f;
        float COURSE_USD_Pumb = 3f;
        float COURSE_USD_Oshad = 4f;

        //init scanner
        Scanner scan = new Scanner(System.in);

        //enter amount of money
        System.out.println("Enter the amount of money that you want to change: ");
        int amount = scan.nextInt();

        //enter currency
        System.out.println("Enter the currency Bank (Privat, Pumb, Oshad): ");
        String currencyBank = scan.next();

        //convert uan to user currency
        if (privat.equalsIgnoreCase(currencyBank)) {
            System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / COURSE_USD_Privat));
        } else if (pumb.equalsIgnoreCase(currencyBank)) {
            System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / COURSE_USD_Pumb));
        } else if (oshad.equalsIgnoreCase(currencyBank)) {
            System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / COURSE_USD_Oshad));
        } else System.err.println("Can't convert to " + currencyBank);

    }

}
