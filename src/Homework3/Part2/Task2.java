package Homework3.Part2;

import java.util.Locale;
import java.util.Scanner;

public class Task2 {

    /**
     * This program convert UAN to USD or EUR
     */
    public static void main(String[] args) {

        //init variables
        String USD = "USD";
        String EUR = "EUR";
        String RUB = "RUB";

        //init Banks
        String privat = "Privat";
        String pumb = "Pumb";
        String oshad = "Oshad";

        float COURSE_USD_Privat = 2f;
        float COURSE_EUR_Privat = 3f;
        float COURSE_RUB_Privat = 4f;
        float COURSE_USD_Pumb = 5f;
        float COURSE_EUR_Pumb = 6f;
        float COURSE_RUB_Pumb = 7f;
        float COURSE_USD_Oshad = 8f;
        float COURSE_EUR_Oshad = 9f;
        float COURSE_RUB_Oshad = 10f;

        //init scanner
        Scanner scan = new Scanner(System.in);

        //enter amount of money
        System.out.println("Enter the amount of money that you want to change: ");
        int amount = scan.nextInt();

        //enter currencyBank
        System.out.println("Enter the currency Bank (Privat, Pumb or Oshad): ");
        String currencyBank = scan.next();

        //enter currency
        System.out.println("Enter the currency Bank (USD, EUR or RUB): ");
        String currency = scan.next();

        //convert uan to user currency
        if (privat.equalsIgnoreCase(currencyBank)) {
            if (USD.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / COURSE_USD_Privat));
            } else if (EUR.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", EUR, amount / COURSE_EUR_Privat));
            } else if (RUB.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", RUB, amount / COURSE_RUB_Privat));
            } else System.err.println("Can't convert to " + currency);

        } else if (pumb.equalsIgnoreCase(currencyBank)) {
            if (USD.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / COURSE_USD_Pumb));
            } else if (EUR.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", EUR, amount / COURSE_EUR_Pumb));
            } else if (RUB.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", RUB, amount / COURSE_RUB_Pumb));
            } else System.err.println("Can't convert to " + currency);
        } else if (oshad.equalsIgnoreCase(currencyBank)) {

            if (USD.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / COURSE_USD_Oshad));
            } else if (EUR.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", EUR, amount / COURSE_EUR_Oshad));
            } else if (RUB.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", RUB, amount / COURSE_RUB_Oshad));
            } else System.err.println("Can't convert to " + currency);
        } else System.err.println("Invalid bank: " + currencyBank);

    }

}
