package Homework5;


import java.util.Arrays;

public class Book {
    public String name;
    public String[] author;
    public int year;
    public String publishingHouse;


    public Book(String name, String[] author, int year, String publishingHouse) {
        this.name = name;
        this.author = author;
        this.year = year;
        this.publishingHouse = publishingHouse;
    }

    @Override
    public String toString() {
        return name + ", написанная " + Arrays.toString(author) + " " + year + " года";
    }

    public Book() {
    }
}


