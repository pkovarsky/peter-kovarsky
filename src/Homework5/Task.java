package Homework5;

public class Task {

    static Book[] books = new Book[4];

    public static void main(String[] args) {

        initBooks();
        Book oldBook = books[0];
        for (int i = 0; i < books.length; i++) {
            if (books[i].year < oldBook.year) {
                oldBook = books[i];
            }
        }

        System.out.println("Книги автора Vladimir : ");
        for (int i = 0; i < books.length; i++) {
            for (int j = 0; j < books[i].author.length; j++) {
                if ("Vladimir".equalsIgnoreCase(books[i].author[j])) {
                    System.out.println(books[i].name);
                }
            }
        }


        System.out.println();
        for (int i = 0; i < books.length; i++) {
            if (books[i].year < 2000) {
                System.out.println("Книга " + books[i].toString() + " издана ранее 2000 года");
            }
        }

        System.out.println("\n" + "Самая старая книга автора " + oldBook.author[0] + ", написанна в " + oldBook.year + " году.");


        System.out.println("\n" + "Книги Vladimir в соавторстве с другими писателями: ");
        for (int i = 0; i < books.length; i++) {
            for (int j = 0; j < books[i].author.length; j++) {
                if ("Vladimir".equalsIgnoreCase(books[i].author[j])) {
                    if (books[i].author.length > 1) {
                        System.out.println(books[i].toString());
                    }
                }
            }
        }

        System.out.println("\n" + "Книги, где 3 и более авторов: ");
        for (int i = 0; i < books.length; i++) {
            if (books[i].author.length >= 3) {
                System.out.println(books[i].toString());
            }
        }
    }


    static void initBooks() {
        books[0] = new Book("West", new String[]{"Vladimir", "Kek"}, 1998, "1");
        books[1] = new Book("East", new String[]{"Bogdan", "Sergey", "Anton"}, 1997, "51");
        books[2] = new Book("South", new String[]{"Vladimir"}, 2003, "3");
        books[3] = new Book("North", new String[]{"Vladimir", "Sveta"}, 1999, "2");
    }
}
