package Homework2;

import java.util.Scanner;


public class Task3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Сколько строк вы будете вводить? \n ");
        int n = in.nextInt();
        if (0>=n){
            while( 0>=n){
                System.err.println("Количество строк должно быть больше нуля!");
                System.out.print("Введите число строк заново \n ");
                n = in.nextInt();
            }
        }
        String m = "";
        String[] strings;
        strings = new String[n];
        for (int i = 0; i < n; ++i) {
            System.out.println("Введите строку номер " + (i+1));
            strings[i] = in.next();
        }
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - 1 - i; j++) {
                if (strings[j].length() > strings[j + 1].length()) {
                    m = strings[j];
                    strings[j] = strings[j + 1];
                    strings[j + 1] = m;
                }
            }
        }
        System.out.println("Строка с минимальным количеством символов: " + "\n" + strings[0] + "\n" + "Количество символов " +
                "символов в этой строке = " + strings[0].length());
    }
}



