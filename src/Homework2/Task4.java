package Homework2;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Сколько строк вы будете вводить? \n ");

        int n = in.nextInt();

        while (n <= 0) {
            System.err.println("Количество строк должно быть больше нуля! ");
            System.out.print("Введите число строк заново \n  ");

            n = in.nextInt();
        }

        String[] strings = new String[n + 1];

        for (int i = 0; i < n; i++) {
            System.out.println("Введите строку номер " + (i+1));
            strings[i] = in.next();
        }
        int sum = 0;
        for (int i = 0; i < n; i++){
            sum += strings[i].length();
        }
        for (int i = 0; i<n; i++){
            if(strings[i].length()<sum/n){
                System.out.println("Строки, длина которых меньше средней длины, а также их длина: " + strings[i] + " " + strings[i].length());
            }
        }
    }
}







