package Homework2;

import java.util.Scanner;

/**
 * Created by p.kovarsky on 11/30/16.
 */
public class Task2 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        double x1 = 0;
        double x2 = 0;
        System.out.print("Введите a: ");
        double a1 = in.nextDouble();
        System.out.print("Введите b: ");
        double b1 = in.nextDouble();
        System.out.print("Введите c: ");
        double c1 = in.nextDouble();

        if (a1 == 0 && b1 == 0) {
            for (int i = 0; a1 == 0 && b1 == 0; i++) {
                System.err.println("\n" + "Введите \"а\" и \"b\" не равные нулю:");
                System.out.print("\n" + "Введите \"a\" еще раз: ");
                a1 = in.nextDouble();
                System.out.print("\n" + "Введите \"b\" еще раз: ");
                b1 = in.nextDouble();
            }
        }
        if (a1 == 0d) {
            x1 = -c1 / b1;
            System.out.println("X = " + x1);
        } else {
            if (b1 == 0d) {
                if (-c1 / a1 > 0) {
                    x1 = Math.sqrt(-c1 / a1);
                    x2 = -Math.sqrt(-c1 / a1);
                    System.out.println("\n" + "X1: " + x1 + "\n" + "X2: " + x2);
                } else {
                    System.err.println("\n" + "Корней на множестве действительных чисел нет");
                }
            } else {
                double d = b1 * b1 - 4 * a1 * c1;
                if (d > 0d) {
                    x1 = (-b1 + Math.sqrt(d)) / (2 * a1);
                    x2 = (-b1 - Math.sqrt(d)) / (2 * a1);
                    System.out.println("\n" + "X1: " + x1 + "\n" + "X2: " + x2);
                } else {
                    if (d == 0d) {
                        x1 = -b1 / (2 * a1);
                        x2 = x1;
                        System.out.println("\n" + "X1 = X2 = " + x1);
                    } else {
                        System.err.println("\n" + "Корней на множестве действительных чисел нет");
                    }
                }
            }
        }
    }
}